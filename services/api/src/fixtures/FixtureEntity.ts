import {
  BaseEntity,
  Entity,
  ManyToOne,
  PrimaryKey,
  Property,
} from '@mikro-orm/core';
import { v4 } from 'uuid';
import { FixtureTypeEntity } from './FixtureTypeEntity.js';

@Entity()
export class FixtureEntity extends BaseEntity<FixtureEntity, 'id'> {
  @PrimaryKey()
  public id: string = v4();

  @Property()
  public universe = 0;

  @Property()
  public address = 0;

  @ManyToOne(() => FixtureTypeEntity, { eager: true })
  public fixtureType!: FixtureTypeEntity;
}
