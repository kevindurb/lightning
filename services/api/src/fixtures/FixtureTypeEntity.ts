import {
  BaseEntity,
  Entity,
  JsonType,
  PrimaryKey,
  Property,
} from '@mikro-orm/core';
import { v4 } from 'uuid';

export enum ChannelType {
  Intensity = 'Intensity',
  Red = 'Red',
  Green = 'Green',
  Blue = 'Blue',
  Cyan = 'Cyan',
  Magenta = 'Magenta',
  Yellow = 'Yellow',
}

export type Channel = {
  offset: number;
  type: ChannelType;
};

@Entity()
export class FixtureTypeEntity extends BaseEntity<FixtureTypeEntity, 'id'> {
  @PrimaryKey()
  public id: string = v4();

  @Property()
  public name = '';

  @Property({ type: JsonType })
  public channels: Channel[] = [];
}
