import Router from '@koa/router';
import type { AppState, AppContext } from '../Context.js';
import { Expose } from 'class-transformer';
import { fromPOJO } from '../core/fromPOJO.js';
import {
  IsIn,
  IsNotEmpty,
  IsPositive,
  IsUUID,
  ValidateNested,
  validateOrReject,
} from 'class-validator';
import httpErrors from 'http-errors';
import { ChannelType } from './FixtureTypeEntity.js';

export const FixtureController = new Router<AppState, AppContext>({});

class CreateFixtureInput {
  @Expose()
  universe!: number;

  @Expose()
  address!: number;

  @IsUUID()
  @Expose()
  fixtureTypeId!: string;
}

class ChannelInput {
  @Expose()
  @IsPositive()
  offset!: number;

  @Expose()
  @IsIn(Object.values(ChannelType))
  type!: ChannelType;
}

class CreateFixtureTypeInput {
  @Expose()
  @IsNotEmpty()
  name!: string;

  @Expose()
  @ValidateNested()
  channels!: ChannelInput[];
}

FixtureController.get('/fixtures', async (ctx) => {
  const fixtures = await ctx.fixtureRepository.findAll();

  ctx.body = fixtures.map((fixture) => fixture.toPOJO());
});

FixtureController.get('/fixture-types', async (ctx) => {
  const fixtureTypes = await ctx.fixtureTypeRepository.findAll();

  ctx.body = fixtureTypes.map((fixtureType) => fixtureType.toPOJO());
});

FixtureController.post('/fixtures', async (ctx) => {
  const input = fromPOJO(CreateFixtureInput, ctx.request.body);
  await validateOrReject(input);

  const fixtureType = await ctx.fixtureTypeRepository.findOne({
    id: input.fixtureTypeId,
  });
  if (!fixtureType) throw new httpErrors.BadRequest();

  const fixture = ctx.fixtureRepository.create({
    universe: input.universe,
    address: input.address,
    fixtureType,
  });

  ctx.body = fixture.toPOJO();
});

FixtureController.post('/fixture-types', async (ctx) => {
  const input = fromPOJO(CreateFixtureTypeInput, ctx.request.body);
  console.log(input);
  await validateOrReject(input);

  const fixtureType = ctx.fixtureTypeRepository.create({
    name: input.name,
    channels: input.channels,
  });

  ctx.body = fixtureType.toPOJO();
});
