import { MikroORM } from '@mikro-orm/core';

import config from './mikro-orm.config.js';

let orm: MikroORM;

export const getOrm = async () => {
  if (!orm) {
    orm = await MikroORM.init(config);
  }
  return orm;
};
