import 'reflect-metadata';
import 'dotenv/config';

import { getApp } from './app.js';
import { getConfig } from './config.js';
import { getOrm } from './database.js';

const orm = await getOrm();

const config = getConfig();

if (config.ensureSchema) {
  await orm.getSchemaGenerator().ensureDatabase();
  await orm.getSchemaGenerator().updateSchema();
}

(await getApp()).listen(config.listenAddress, () => {
  console.log(`Listening on ${config.listenAddress}`);
});
