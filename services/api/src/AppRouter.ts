import Router from '@koa/router';

import { AppContext, AppState } from './Context.js';
import { FixtureController } from './fixtures/FixtureController.js';

export const AppRouter = new Router<AppState, AppContext>();

AppRouter.use(FixtureController.routes(), FixtureController.allowedMethods());
