import { ClassConstructor, plainToClass } from 'class-transformer';

export const fromPOJO = <T, V>(Class: ClassConstructor<T>, pojo: V) => {
  return plainToClass<T, V>(Class, pojo, {
    excludeExtraneousValues: true,
  });
};
