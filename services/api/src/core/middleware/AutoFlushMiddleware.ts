import { AppMiddleware } from '../../Context.js';

export const AutoFlushMiddleware: AppMiddleware = async (ctx, next) => {
  await next();
  if (ctx.method !== 'GET') await ctx.em.flush();
};
