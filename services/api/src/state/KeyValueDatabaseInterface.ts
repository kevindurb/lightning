export interface KeyValueDatabaseInterface {
  set(key: string, value: string): void;

  get(key: string): string | undefined;

  unset(key: string): void;
}
