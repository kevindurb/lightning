import { KeyValueDatabaseInterface } from './KeyValueDatabaseInterface.js';

export class StateMemoryDatabase implements KeyValueDatabaseInterface {
  private data: Record<string, string> = {};

  public set(key: string, value: string) {
    this.data[key] = value;
  }

  public get(key: string) {
    return this.data[key];
  }

  unset(key: string) {
    delete this.data[key];
  }
}
