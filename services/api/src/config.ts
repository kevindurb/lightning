import type { Options, EntityClass } from '@mikro-orm/core';
import { FixtureEntity } from './fixtures/FixtureEntity.js';
import { FixtureTypeEntity } from './fixtures/FixtureTypeEntity.js';

const entities: EntityClass<unknown>[] = [FixtureEntity, FixtureTypeEntity];

export type Config = {
  listenAddress: string;
  environment: Environment;
  mikroOrm: Options;
  ensureSchema: boolean;
};

export type Environment = 'development' | 'test' | 'production';

const environmentConfigs: Record<Environment, Config> = {
  development: {
    listenAddress: '8000',
    environment: 'development',
    ensureSchema: true,
    mikroOrm: {
      type: 'sqlite',
      persistOnCreate: true,
      entities,
      dbName: './db.sqlite',
      migrations: { disableForeignKeys: false },
    },
  },
  test: {
    listenAddress: '8000',
    environment: 'test',
    ensureSchema: false,
    mikroOrm: {
      type: 'sqlite',
      persistOnCreate: true,
      entities,
      dbName: ':memory:',
      migrations: { disableForeignKeys: false },
    },
  },
  production: {
    listenAddress: '80',
    environment: 'production',
    ensureSchema: false,
    mikroOrm: {
      persistOnCreate: true,
      entities,
      dbName: 'acouplequestions',
      type: 'postgresql',
      clientUrl: process.env['POSTGRES_URL'] ?? '',
      migrations: { disableForeignKeys: false },
    },
  },
};

export const getConfig = () =>
  environmentConfigs[
    (process.env['NODE_ENV'] as Environment | undefined) ?? 'development'
  ];
