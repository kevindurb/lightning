import { bodyParser } from '@koa/bodyparser';
import cors from '@koa/cors';
import { ValidationError } from 'class-validator';
import httpErrors from 'http-errors';
import Koa from 'koa';
import logger from 'koa-logger';
import statuses from 'statuses';

import { AppRouter } from './AppRouter.js';
import { ContextMiddleware } from './Context.js';
import { AutoFlushMiddleware } from './core/middleware/AutoFlushMiddleware.js';

export const getApp = async () => {
  const app = new Koa();
  app.use(logger());

  app.use(async (ctx, next) => {
    try {
      await next();
    } catch (error) {
      if (error instanceof ValidationError) {
        ctx.status = 400;
        ctx.body = { error: error.toString() };
      } else if (httpErrors.isHttpError(error)) {
        ctx.status = error.status;
        if (error.expose) ctx.body = { error: error.message };
        else ctx.body = statuses(error.status);
      } else {
        console.error(error);
        ctx.status = 500;
        ctx.body = statuses(500);
      }
    }
  });

  app.use(cors());
  app.use(bodyParser());
  app.use(ContextMiddleware);
  app.use(AutoFlushMiddleware);
  app.use(AppRouter.routes());
  app.use(AppRouter.allowedMethods());

  return app;
};
