import { EntityManager, MikroORM } from '@mikro-orm/core';
import Koa from 'koa';

import { Config, getConfig } from './config.js';
import { getOrm } from './database.js';
import { EntityRepository } from '@mikro-orm/postgresql';
import { FixtureEntity } from './fixtures/FixtureEntity.js';
import { FixtureTypeEntity } from './fixtures/FixtureTypeEntity.js';
import { StateService } from './state/StateService.js';
import { StateMemoryDatabase } from './state/StateMemoryDatabase.js';

export type AppState = {};

export type AppContext = {
  config: Config;
  orm: MikroORM;
  em: EntityManager;
  fixtureRepository: EntityRepository<FixtureEntity>;
  fixtureTypeRepository: EntityRepository<FixtureTypeEntity>;
  stateService: StateService;
};

export type AppMiddleware = Koa.Middleware<AppState, AppContext>;

export const ContextMiddleware: AppMiddleware = async (ctx, next) => {
  const orm = await getOrm();
  ctx.config = getConfig();
  ctx.orm = orm;
  ctx.em = orm.em.fork();

  ctx.fixtureRepository = ctx.em.getRepository(FixtureEntity);
  ctx.fixtureTypeRepository = ctx.em.getRepository(FixtureTypeEntity);
  ctx.stateService = new StateService(new StateMemoryDatabase());

  await next();
};
