import { render } from 'preact';

const root = document.getElementById('root');
if (!root) throw new Error('Missing root element');

render('hello world', root);
